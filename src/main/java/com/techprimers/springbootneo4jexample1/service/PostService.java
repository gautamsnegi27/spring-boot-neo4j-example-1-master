package com.techprimers.springbootneo4jexample1.service;

import com.techprimers.springbootneo4jexample1.model.Post;
import com.techprimers.springbootneo4jexample1.model.User;
import com.techprimers.springbootneo4jexample1.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class PostService {

    @Autowired
    PostRepository postRepository;
    public Post save(Post post)
    {
        return postRepository.save(post);
    }

    public Post get(Long postId)
    {
        try {
            return postRepository.findOne(postId);
        }catch (Exception e)
        {
            System.out.println("get: "+ e);
            return null;
        }
    }

    public void deletePost(Post post)
    {
        postRepository.delete(post.getPostId());
    }




}
