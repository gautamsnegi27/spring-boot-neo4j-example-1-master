package com.techprimers.springbootneo4jexample1.service;

import com.techprimers.springbootneo4jexample1.model.Reply;
import com.techprimers.springbootneo4jexample1.repository.ReplyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReplyService {
    @Autowired
    ReplyRepository replyRepository;

    public Reply save(Reply reply)
    {
        return replyRepository.save(reply);
    }

    public void deleteReply(Reply reply){ replyRepository.delete(reply.getReplyId());}
}
