package com.techprimers.springbootneo4jexample1.service;

import com.techprimers.springbootneo4jexample1.model.Like;
import com.techprimers.springbootneo4jexample1.model.Post;
import com.techprimers.springbootneo4jexample1.model.User;
import com.techprimers.springbootneo4jexample1.repository.LikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LikeService {
    @Autowired
    LikeRepository likeRepository;

    public void delete(Like like)
    {
        likeRepository.delete(like);
    }
    public Like save(Like like)
    {
        return likeRepository.save(like);
    }

    public void likeAndEstRln(Like like, Post post){
        likeRepository.likeandestrln(like.getId(),post.getPostId());
    }

    public User findUserId(Post post){
        return likeRepository.findUserId(post.getPostId());
    }

    public Like findByPostId(Long postId,String userId)
    {
        return likeRepository.findByPostId(postId,userId);
    }

}
