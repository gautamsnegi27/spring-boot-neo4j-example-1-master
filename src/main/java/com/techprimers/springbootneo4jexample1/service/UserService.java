package com.techprimers.springbootneo4jexample1.service;

import com.techprimers.springbootneo4jexample1.model.Post;
import com.techprimers.springbootneo4jexample1.model.User;
import com.techprimers.springbootneo4jexample1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public Collection<User> getAll() {
        return userRepository.getAllUsers();
    }

    public User save(User user)
    {
        return userRepository.save(user);
    }

    public User get(String userId)
    {
        return userRepository.getUser(userId);
    }

    public User findOne(Long id)
    {
        return userRepository.findOne(id);
    }

    public void estRln(Long id, String userId){
        userRepository.setRelation(id, userId);
    }

    public Post estRlnComment(Long commentId, Long postId)
    {
        try {
            Post post= userRepository.setCommentRelation(commentId,postId);
            return post;
        }
        catch (Exception e)
        {
            System.out.println("Inside UserService estRln:"+ e);
            return null;
        }
    }

    public void estRlnReply(Long commentId, Long replyId)
    {
        userRepository.setReplyRelation(commentId,replyId);
    }

    public User estfriendrln(User user1, User user){
        return userRepository.setRelation(user1.getUserId(),user.getUserId());
    }

    public User checkRelation(User user1, User user){
        return userRepository.checkRelation(user1.getUserId(),user.getUserId());
    }

    public void deleteRelation(User user, User user1){
        userRepository.deleteRelation(user.getUserId(),user1.getUserId());
    }


    public void deleteUser(User user)
    {
        userRepository.delete(user);
    }

    public List<User> getFollowers(User user){
            return userRepository.getFollowers(user.getUserId());
    }

    public List<User> getFollowing(User user){
        return userRepository.getFollowing(user.getUserId());
    }

    public List<User> getUserProfile(User user){
        try {
            return userRepository.getUserProfile(user.getUserId());
        }
        catch (Exception e)
        {
            System.out.println("Exception handled : "+e);
            return null;
        }
    }

    public List<User> getFeed(String userId){
        try
        {
            return userRepository.getFeed(userId);
        }
        catch (Exception e)
        {
            System.out.println("Exception handled : "+e);
            return null;
        }
    }


}
