package com.techprimers.springbootneo4jexample1.model;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.List;

@NodeEntity
public class Post {

    @GraphId
    private Long postId;
    private String postData;
    private String dateAndTime;
    private String mediaUrl;

    @Relationship(type = "COMMENTED", direction = Relationship.INCOMING)
    private List<Comment> comments;
    @Relationship(type = "LIKED", direction = Relationship.INCOMING)
    private List<Like> likes;

    public Post() {
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getPostData() {
        return postData;
    }

    public void setPostData(String postData) {
        this.postData = postData;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Like> getLikes() {
        return likes;
    }

    public void setLikes(List<Like> likes) {
        this.likes = likes;
    }

    public String getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(String dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    @Override
    public String toString() {
        return "Post{" +
                "postId=" + postId +
                ", postData='" + postData + '\'' +
                ", dateAndTime='" + dateAndTime + '\'' +
                ", comments=" + comments +
                ", likes=" + likes +
                '}';
    }
}
