package com.techprimers.springbootneo4jexample1.repository;

import com.techprimers.springbootneo4jexample1.model.Post;
import com.techprimers.springbootneo4jexample1.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

@Repository
public interface UserRepository extends Neo4jRepository<User, Long> {

    @Query("MATCH (u:User)<-[r:POSTED]-(p:Post) RETURN p,r,u")
    Collection<User> getAllUsers();

    @Query("MATCH (u:User),(p:Post) WHERE u.userId={userId} AND id(p)={id} CREATE (p)-[r:POSTED]->(u) RETURN r")
    void setRelation(@Param("id") Long id, @Param("userId") String userId);

    @Query("MATCH (u:User) WHERE u.userId ={userId} RETURN u")
    User findUser(@Param("userId") String userId);

    @Query("MATCH (c:Comment),(p:Post) WHERE id(c)={commentId} AND " +
            "id(p)={postId} CREATE (c)-[r:COMMENTED]->(p) RETURN p,c")
    Post setCommentRelation(@Param("commentId") Long commentId, @Param("postId") Long postId);


    @Query("MATCH (c:Comment),(re:Reply) WHERE id(c)={commentId} AND " +
            "id(re)={replyId} CREATE (re)-[r:REPLIED]->(c) RETURN r")
    void setReplyRelation(@Param("commentId") Long commentId, @Param("replyId") Long replyId);

    @Query("MATCH (u:User) WHERE u.userId={userId} RETURN u")
    User getUser(@Param("userId") String userId);

    @Query("MATCH (u:User),(u1:User) WHERE u.userId={userId} AND u1.userId={userId1} " +
            "CREATE (u)<-[r:FOLLOWS]-(u1) RETURN u")
    User setRelation(@Param("userId") String userId,
                     @Param("userId1") String userId1);

    @Query("match (u:User)<-[f:FOLLOWS]-(u1:User) " +
            "where u.userId = {userId} and u1.userId = {userId1} return u")
    User checkRelation(@Param("userId") String userId,
                     @Param("userId1") String userId1);



    @Query("match (u:User)<-[f:FOLLOWS]-(u1:User) " +
            "where u.userId = {userId} and u1.userId = {userId1} delete f;")
    void deleteRelation(@Param("userId") String userId,
                     @Param("userId1") String userId1);

    @Query("MATCH (u:User)<-[r:FOLLOWS]-(u1:User) where u.userId = {userId} RETURN u1")
    List<User> getFollowers(@Param("userId") String userId);

    @Query("MATCH (u:User)<-[r:FOLLOWS]-(u1:User) where u.userId = {userId} RETURN u,r,u1")
    List<User> getFollowing(@Param("userId") String userId);

    @Query("match (u:User) where u.userId = {userId}"+
            "Optional MATCH (u)<-[r1:POSTED]-(p:Post)" +
            "Optional MATCH (u)<-[r1]-(p)<-[r2:COMMENTED]-(c:Comment)" +
            "Optional MATCH (u)<-[r1]-(p:Post)<-[r2]-(c)<-[r3:REPLIED]-(r:Reply)" +
            "Optional MATCH (u)<-[r1]-(p)<-[r4:LIKED]-(l:Like)" +
            "optional Match (u)<-[f:FOLLOWS]-(u1:User)" +
            "optional Match (u1:User)<-[f:FOLLOWS]-(u)" +
            "RETURN distinct p,r,u,u1,r1,r2,r3,r4,l,f,c order by p.dateAndTime desc")
    List<User> getUserProfile(@Param("userId") String userId);

    @Query("match (u:User) where u.userId = {userId}" +
            "optional Match (u)-[r:FOLLOWS]->(u1:User)" +
            "Optional MATCH (u1)<-[r1:POSTED]-(p1:Post)" +
            "Optional MATCH (u1)<-[r1]-(p1)<-[r2:COMMENTED]-(c:Comment)" +
            "Optional MATCH (u1)<-[r1]-(p1)<-[r2]-(c)<-[r3:REPLIED]-(y:Reply)" +
            "Optional MATCH (u1)<-[r1]-(p)<-[r4:LIKED]-(l:Like)" +
            "RETURN distinct u,u1,r1,r2,r3,r4,p1,y,l,c order by p1.dateAndTime desc")
    List<User> getFeed(@Param("userId") String userId);
}
