package com.techprimers.springbootneo4jexample1.repository;

import com.techprimers.springbootneo4jexample1.model.Like;
import com.techprimers.springbootneo4jexample1.model.User;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface LikeRepository extends Neo4jRepository<Like,Long> {
    @Query("MATCH (l:Like),(p:Post) WHERE id(l)={likeId} AND " +
            "id(p)={postId} CREATE (l)-[r:LIKED]->(p) RETURN r")
    void likeandestrln(@Param("likeId") Long likeId,@Param("postId") Long postId);

    @Query("match (u:User)<-[r:POSTED]-(p:Post)<-[l:LIKED]-(ll:Like)  " +
            "where id(p) = {postId} and ll.userId = {userId} return ll")
    Like findByPostId(@Param("postId") Long postId,@Param("userId") String userId);

    @Query("match (u:User)<-[r:POSTED]-(p:Post) where id(p)={postId} return u")
    User findUserId(@Param("postId") Long postId);
}
