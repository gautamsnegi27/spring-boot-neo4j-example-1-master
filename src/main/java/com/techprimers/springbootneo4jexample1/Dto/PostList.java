package com.techprimers.springbootneo4jexample1.Dto;

import com.techprimers.springbootneo4jexample1.model.Comment;
import com.techprimers.springbootneo4jexample1.model.Like;
import javafx.geometry.Pos;

import java.util.List;

public class PostList implements Comparable<PostList> {
    private Long postId;
    private String postData;
    private String dateAndTime;
    private List<Comment> comments;
    private List<Like> likes;
    private String userId;
    private String userName;
    private String userAvatar;

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getPostData() {
        return postData;
    }

    public void setPostData(String postData) {
        this.postData = postData;
    }

    public String getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(String dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Like> getLikes() {
        return likes;
    }

    public void setLikes(List<Like> likes) {
        this.likes = likes;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public int compareTo(PostList o) {
        return o.dateAndTime.compareTo(dateAndTime);
    }
}
