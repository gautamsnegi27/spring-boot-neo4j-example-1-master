package com.techprimers.springbootneo4jexample1.Dto;

public class PostAndUserDto {
    private Long postId;
    private String postData;
    private String userId;
    private Long commentId;
    private String userName;
    private String userAvatar;
    private String commentUserId;
    private String commentData;
    private String replyData;
    private Integer likeCount;
    private int pageOffSet;
    private boolean flag;

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public int getPageOffSet() {
        return pageOffSet;
    }

    public void setPageOffSet(int pageOffSet) {
        this.pageOffSet = pageOffSet;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public String getPostData() {
        return postData;
    }

    public void setPostData(String postData) {
        this.postData = postData;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getCommentUserId() {
        return commentUserId;
    }

    public void setCommentUserId(String commentUserId) {
        this.commentUserId = commentUserId;
    }

    public String getCommentData() {
        return commentData;
    }

    public void setCommentData(String commentData) {
        this.commentData = commentData;
    }

    public String getReplyData() {
        return replyData;
    }

    public void setReplyData(String replyData) {
        this.replyData = replyData;
    }


    @Override
    public String toString() {
        return "PostAndUserDto{" +
                "postId=" + postId +
                ", postData='" + postData + '\'' +
                ", userId='" + userId + '\'' +
                ", commentId=" + commentId +
                ", userName='" + userName + '\'' +
                ", userAvatar='" + userAvatar + '\'' +
                ", commentUserId='" + commentUserId + '\'' +
                ", commentData='" + commentData + '\'' +
                ", replyData='" + replyData + '\'' +
                ", likeCount=" + likeCount +
                ", pageOffSet=" + pageOffSet +
                ", flag=" + flag +
                '}';
    }
}
