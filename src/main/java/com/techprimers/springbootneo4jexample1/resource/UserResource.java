package com.techprimers.springbootneo4jexample1.resource;

import com.techprimers.springbootneo4jexample1.Dto.*;
import com.techprimers.springbootneo4jexample1.model.*;
import com.techprimers.springbootneo4jexample1.service.*;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@CrossOrigin("*")
public class UserResource {

//============================    Repositories    ============================

    @Autowired
    private UserService userService;

    @Autowired
    private PostService postService;


    @Autowired
    private CommentService commentService;

    @Autowired
    private ReplyService replyService;

    @Autowired
    LikeService likeService;

//============================    getFunctions    ============================

    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j")
    public Collection<User> getAll() {
        return userService.getAll();
    }

    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/getuser")
    public User getUser(@RequestBody User user) {
        return userService.get(user.getUserId());
    }

    public User getUser(String userId) {
        return userService.get(userId);
    }

    @RequestMapping(method = RequestMethod.POST,value = "/rest/neo4j/getPost")
    public Post getPost(@RequestBody Post post) {
        try {
            return postService.get(post.getPostId());
        }catch (Exception e)
        {
            System.out.println("could not find the post "+ e);
            return null;
        }
    }

    public Post getPost(@RequestBody Long postId) {
        return postService.get(postId);
    }
    @RequestMapping(method = RequestMethod.POST,value = "/rest/neo4j/getComment")
    public Comment getComment(@RequestBody Comment comment) {
        return commentService.getComment(comment.getCommentId());
    }

//============================    getListFunctions    ============================

    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/getuserprofile" )
    public ResponseEntity<?> getUserProfile(@RequestBody PostAndUserDto postAndUserDto) {
        User user=getUser(postAndUserDto.getUserId());
        List<User> users = userService.getUserProfile(user);
        if(null==users)
        {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        }
        List<Post> posts = users.get(0).getPosts();
        int start=postAndUserDto.getPageOffSet()*3;
        if(start>posts.size())
        {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        }
        int to=postAndUserDto.getPageOffSet()*3+3;
        if(to>=posts.size()){
            to=posts.size();
        }
        return new ResponseEntity<>(posts.subList(start,to), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/getfeed" )
    public ResponseEntity<?> getFeed(@RequestBody PostAndUserDto postAndUserDto) {
        User user=getUser(postAndUserDto.getUserId());
        List<User> users;
        List<User> user0 = userService.getUserProfile(user);
        List<PostList> postList = new ArrayList<>();
        List<User> user00 = userService.getFeed(postAndUserDto.getUserId());
        if(user0==null&& user00 == null)
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        else if(user0==null)
            users=user00;
        else if(user00==null)
            users=user0;
        else
            users = ListUtils.union(user0, user00);

        for (User user2 : users) {
            PostList postList1 = new PostList();
            BeanUtils.copyProperties(user2, postList1);
            if (user2.getPosts() != null) {
                for (Post post : user2.getPosts()) {
                    BeanUtils.copyProperties(post, postList1);
                    postList.add(postList1);
                    postList1 = new PostList();
                    BeanUtils.copyProperties(user2, postList1);
                }
            }
        }

        if(postList.size()==0)
        {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        }
        Collections.sort(postList);

        int start = postAndUserDto.getPageOffSet() * 3;
        if (start > postList.size())
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        int to = postAndUserDto.getPageOffSet() * 3 + 3;
        if (to >= postList.size()) {
            to = postList.size();
        }
        return new ResponseEntity<>(postList.subList(start, to), HttpStatus.OK);

    }


//    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/getuserdetail")
//    public List<?> getUserDetail(@RequestBody PostAndUserDto postAndUserDto) {
//        User user=getUser(postAndUserDto.getUserId());
//
//        System.out.println("getuserdetails: "+postAndUserDto.getUserId());
//        return userService.getUserProfile(user).get(0).getFollowers();
//    }


    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/getfollowerlist" )
    public ResponseEntity<?> getFollowerList(@RequestBody FriendDto friendDto) {
        User user=getUser(friendDto.getUserId());
        List<User> users = userService.getFollowers(user);
        if (users.size()>1)
            return new ResponseEntity<>(users, HttpStatus.OK);
        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/getfollowinglist" )
    public ResponseEntity<?> getFollowingList(@RequestBody FriendDto friendDto) {
        User user=getUser(friendDto.getUserId());
        List<User> users = userService.getFollowing(user);
        try {
            if (null != users.get(0).getFollowers())
                return new ResponseEntity<>(users, HttpStatus.OK);
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        }
    }

//============================    insetFunctions    ============================

    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/insertuser" )
    public User insertUser(@RequestBody User user) {
        return userService.save(user);
    }


    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/insertcomment" )
    public Comment insertComment(@RequestBody Comment comment) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        comment.setDateAndTime(formatter.format(new Date()));
        return commentService.save(comment);
    }

    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/insertreply" )
    public Reply insertReply(@RequestBody Reply reply) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        reply.setDateAndTime(formatter.format(new Date()));
        return replyService.save(reply);
    }
    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/insertlike")
    public Like insertlike(@RequestBody Like like) {
        return likeService.save(like);
    }

//============================    establishRelation    ============================

    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/postandestrln" )
    public ResponseEntity<?> postAndestRln(@RequestBody PostAndUserDto postAndUserDto) {
        Post post = new Post();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        post.setDateAndTime(formatter.format(new Date()));
        post.setPostData(postAndUserDto.getPostData());
        try {
            post = postService.save(post);
            if (null != post) {
                User user = getUser(postAndUserDto.getUserId());
                userService.estRln(post.getPostId(), user.getUserId());
                return new ResponseEntity<>(post, HttpStatus.OK);
            }
            throw new Exception("Could not add post");
        }catch (Exception e)
        {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/commentandestrln" )
    public ResponseEntity<?> commentAndEstRln(@RequestBody PostAndUserDto postAndUserDto) {
        try
        {
            Comment comment = new Comment();
            comment.setCommentData(postAndUserDto.getCommentData());
            comment.setUserId(postAndUserDto.getUserId());
            comment = commentService.save(comment);
            Post post = new Post();
            post.setPostId(postAndUserDto.getPostId());
            post = getPost(post);
            User user1 = userService.get(postAndUserDto.getUserId());
            if(null== post)
                throw new Exception("Wrong postId");
            Post post1 = userService.estRlnComment(comment.getCommentId(),post.getPostId());
            if(null!=post1) {
                User user = likeService.findUserId(post);
                LikeNotify likeNotify = new LikeNotify();
                likeNotify.setUserId1(user.getUserId());
                likeNotify.setUserId2(postAndUserDto.getUserId());
                likeNotify.setPostId(post.getPostId());
                likeNotify.setUserName2(user1.getUserName());
                final String url="http://172.16.26.37:8082/getComment";
                RestTemplate restTemplate=new RestTemplate();
                ResponseEntity<?> responseEntity =restTemplate.postForEntity(url, likeNotify, Like.class);
                if(null!=responseEntity.getBody())
                    System.out.println("Comment notificaion sent");
                return new ResponseEntity<>("{\"msg\": \"comment added\"}", HttpStatus.OK);
            }
            else
                throw new Exception("Wrong postId");
        }catch (Exception e)
        {
            System.out.println(e);
            return new ResponseEntity<>("{\"err\": \""+e+"\"}", HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/replyandestrln" )
    public void replyAndEstRln(@RequestBody PostAndUserDto postAndUserDto) {
        Reply reply = new Reply();
        reply.setReplyData(postAndUserDto.getReplyData());
        reply.setUserId(postAndUserDto.getUserId());
        reply = insertReply(reply);
        Comment comment = new Comment();
        comment.setCommentId(postAndUserDto.getCommentId());
        comment = getComment(comment);
        userService.estRlnReply(comment.getCommentId(),reply.getReplyId());
    }

    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/estfriendrln" )
    public ResponseEntity<?> estRln(@RequestBody FriendDto friendDto) {
        User user = getUser(friendDto.getUserId());
        User user1 = getUser(friendDto.getUserId1());
        User user2 = userService.get(friendDto.getUserId1());
        try {
            if (friendDto.isFlag()) {
                if(null!=userService.checkRelation(user, user1))
                    throw new Exception("Already followed");
                else if(null!= userService.estfriendrln(user, user1)) {
                    FollowNotify followNotify= new FollowNotify();
                    followNotify.setUserId1(user.getUserId());
                    followNotify.setUserId2(user1.getUserId());
                    followNotify.setUserName2(user2.getUserName());
                    final String url="http://172.16.26.37:8082/getFollow";
                    RestTemplate restTemplate=new RestTemplate();
                    ResponseEntity<?> responseEntity =restTemplate.postForEntity(url, followNotify, Like.class);
                    if(null!=responseEntity.getBody())
                        System.out.println("Follow notification sent");
                    return new ResponseEntity<>("{\"0\": \"Followed\"}", HttpStatus.OK);
                }
                throw new Exception("Could not follow");
            }
            userService.deleteRelation(user, user1);
            return new ResponseEntity<>("{\"1\": \"Unfollowed\"}", HttpStatus.OK);
        }catch (Exception e)
        {
            System.out.println(e);
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/likeandestrln" )
    public ResponseEntity<?> likeAndEstRln(@RequestBody PostAndUserDto postAndUserDto) {
        Like like = new Like();
        if(postAndUserDto.isFlag()) {
            like.setUserId(postAndUserDto.getUserId());
            like = insertlike(like);
            Post post = getPost(postAndUserDto.getPostId());
            likeService.likeAndEstRln(like, post);
            User user1 = userService.get(postAndUserDto.getUserId());
            User user = likeService.findUserId(post);
            LikeNotify likeNotify = new LikeNotify();
            likeNotify.setUserId1(user.getUserId());
            likeNotify.setUserId2(postAndUserDto.getUserId());
            likeNotify.setPostId(post.getPostId());
            likeNotify.setUserName2(user1.getUserName());
            final String url="http://172.16.26.37:8082/getLike";
            RestTemplate restTemplate=new RestTemplate();
            ResponseEntity<?> responseEntity =restTemplate.postForEntity(url, likeNotify, Like.class);
            if(null!=responseEntity.getBody())
                System.out.println("Like notification sent");
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        }
        like = likeService.findByPostId(postAndUserDto.getPostId(),postAndUserDto.getUserId());
        deleteLike(like);
        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);

    }

//============================    deleteFunctions    ============================

    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/deletepost" )
    public void deletePost(@RequestBody Post post) {
        post = getPost(post);
        postService.deletePost(post);
    }

    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/deletecomment" )
    public void deleteComment(@RequestBody Comment comment) {
        commentService.deleteComment(comment);
    }

    @RequestMapping(method = RequestMethod.POST,value = "rest/neo4j/deletereply" )
    public void deleteComment(@RequestBody Reply reply) {
        replyService.deleteReply(reply);
    }
    @RequestMapping(method = RequestMethod.POST,value = "/rest/neo4j/deleteuser")
    public void deleteuser(@RequestBody User user) {
        user = userService.findOne(user.getId());
        userService.deleteUser(user);
    }

    @RequestMapping(method = RequestMethod.POST,value = "/rest/neo4j/deletelike")
    public void deleteLike(@RequestBody Like like) {
        likeService.delete(like);
    }

}
